#!/bin/bash

# Note 

Run these commands from this folder (catalog) and with `mkdocs.yml` and `app-config.yml` present.

## Test The TechDocs Can Be Generated Locally

To test docs will generate, you can first run them in a server locally (will open browser)...

```bash
npx @techdocs/cli serve:mkdocs --verbose --port 8001
```

## Generate The Techdocs Into A Folder

Requires Node.js, npn, npx and Docker to be available. 

```bash
npx @techdocs/cli generate --source-dir . --output-dir ./techdocs --verbose
```

## Publish The TechDocs To AWS S3 Storage Bucket

> Doesn't work with CloudGate - use your own bucket or min.io with a cert on 443.

To setup S3 see https://backstage.io/docs/features/techdocs/using-cloud-storage#configuring-aws-s3-bucket-with-techdocs

Needs AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY and AWS_REGION and AWS_BUCKET_NAME set as ENV variables...

```bash
npx @techdocs/cli publish --publisher-type awsS3 --storage-name $AWS_BUCKET_NAME --entity default/component/spring-sensors
```

## Configuring TAP To Read From The S3 Bucket

Add the following snippet to `tap-values.yml` under the `tap-gui.app-config` section.

```yaml
    techdocs:
      builder: 'external'
      publisher:
        type: 'awsS3'
        awsS3:
          bucketName: <bucket-name>
          credentials:
            accessKeyId: <key>
            secretAccessKey: <secret>
          region: <aws-region>
          s3ForcePathStyle: false
```

Don't forget to re-register your component in the TAP catalog after applying this change to TAP.