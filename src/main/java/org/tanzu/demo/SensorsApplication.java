package org.tanzu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition( 
    servers = {
       @Server(url = "http://spring-sensors.default.tap.labs.satm.eng.vmware.com/", description = "TAP"),
       @Server(url = "http://localhost:8080", description = "localhost")
    }
) 

@SpringBootApplication
public class SensorsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SensorsApplication.class, args);
    }

}
